import {createSlice} from "@reduxjs/toolkit";

export const favSlice = createSlice({
    name: 'fav',
    initialState: {
        favorites: []
    },
    reducers: {
        getFav(state, action) {
           state = action.payload
        },
        addFav(state, action) {
            state.favorites.push(action.payload);
        },
        removeFav(state, action) {
            state = state.filter(elem => elem !== action.payload)
        }
    }
})

export const {getFav, addFav, removeFav} = favSlice.actions;

// export default favSlice.reducer;