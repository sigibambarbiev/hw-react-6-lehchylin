import {createStore, applyMiddleware} from "redux";
import {combineReducers} from "redux";
import {logger} from "redux-logger/src";
import thunk from "redux-thunk";
import {composeWithDevTools} from "redux-devtools-extension";

import {configureStore} from "@reduxjs/toolkit";

import {productListReducer} from "../reducers/productListReducer";
import {cartReducer} from "../reducers/cartReducer";
import {favoritesReducer} from "../reducers/favoritesReducer";
import {productReducer} from "../reducers/productReducer";
import {onCartStatusReducer} from "../reducers/onCartStatusReducer";
import {onFavoritesStatusReducer} from "../reducers/onFavoritesStatusReducer";

import {favSlice} from "./favSlice";

// const rootReducer = combineReducers({
//     productList: productListReducer,
//     cart: cartReducer,
//     onCartStatus: onCartStatusReducer,
//     favorites: favoritesReducer,
//     onFavoritesStatus: onFavoritesStatusReducer,
//     product: productReducer,
// })

// export const store = createStore(rootReducer,
//     composeWithDevTools(applyMiddleware(logger, thunk))
// );

export const store = configureStore({
    reducer: {
        // favorites: favSlice,
        favorites: favoritesReducer,
        productList: productListReducer,
        cart: cartReducer,
        onCartStatus: onCartStatusReducer,
        onFavoritesStatus: onFavoritesStatusReducer,
        product: productReducer,
    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(logger)
});